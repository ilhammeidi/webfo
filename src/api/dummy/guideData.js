const guideSteps = [
  {
    title: 'Welcome to Enlite Prime',
    label: 'Here is some guide to use this template. Click next to continue and back to previous or click on outside to skip it.',
    imgPath: 'img/guide/guide1.jpg',
  },
  {
    title: 'Dark and Light Mode',
    label: 'Enjoy your eyes with dark/light mode, just switch the option in theme panel or on header.',
    imgPath: 'img/guide/guide2.png',
  },
  {
    title: 'Themes and Layout',
    label: 'Build your template with various themes and layout combination. It easy to adjust following your brand identity.',
    imgPath: 'img/guide/guide3.png',
  },
  {
    title: 'Search Components and Pages',
    label: 'Find any ui component or template page quicker.',
    imgPath: 'img/guide/guide4.png',
  },
  {
    title: 'Code Preview',
    label: 'Get source code easier without open files. Just click showcode button then You will see what a magic behind. It is available for Layout, Form Buttons, UI Components, Chart and Maps category.',
    imgPath: 'img/guide/guide5.png',
  },
];

export default guideSteps;
