module.exports = {
  name: 'Blockchain',
  desc: 'Bungalion | React Template',
  prefix: 'oiron',
  footerText: 'Oiron Theme 2022',
  logoText: 'Oiron Theme',
  projectName: 'Retail',
  url: 'oiron2.indisains.com/retail',
  img: '/static/images/logo-retail.png',
};
