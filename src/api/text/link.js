const link = {
  home: '/',
  dashboard: '/dashboard',
  about: '/about',
  team: '/about/team',
  blog: '/blog',
  blogDetail: '/blog/detail-blog',
  login: '/login',
  register: '/register',
  contact: '/contact',
  contactMap: '/contact/with-map',
  card: '/collection',
  product: '/collection/products',
  productDetail: '/collection/detail-product',
  pricing: '/utils/pricing',
  faq: '/utils/faq',
  maintenance: '/utils/maintenance',
  comingSoon: '/utils/coming-soon',
  profile: '/profile',
  task: '/task',
  email: '/email',
};

export default link;
