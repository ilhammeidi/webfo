/* eslint-disable */
import imgAPI from '~/api/images/imgAPI'
import link from '~/api/text/link'

const sample = [
  {
    name: 'company',
    thumb: imgAPI.ui[3],
    child: [
      {
        name: 'about',
        link: '/'
      },
      {
        name: 'team',
        link: '/'
      },
      {
        name: 'blog',
        link: '/'
      },
      {
        name: 'blog detail',
        link: '/'
      }
    ]
  },
  {
    name: 'Form',
    thumb: imgAPI.ui[1],
    child: [
      {
        name: 'login',
        link: '/'
      },
      {
        name: 'register',
        link: '/'
      },
      {
        name: 'contact',
        link: '/'
      },
      {
        name: 'contact map',
        link: '/'
      }
    ]
  },
  {
    name: 'items',
    thumb: imgAPI.ui[0],
    child: [
      {
        name: 'card',
        link: '/'
      },
      {
        name: 'product',
        link: '/'
      },
      {
        name: 'product detail',
        link: '/'
      }
    ]
  },
  {
    name: 'utilities',
    thumb: imgAPI.ui[2],
    child: [
      {
        name: 'pricing',
        link: '/'
      },
      {
        name: 'faq',
        link: '/'
      },
      {
        name: 'maintenance',
        link: '/'
      },
      {
        name: 'coming soon',
        link: '/'
      },
      {
        name: 'error',
        link: '/error'
      }
    ]
  }
]

export default sample
