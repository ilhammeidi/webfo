import en from './en';
import id from './id';
import ar from './ar';
import zh from './zh';
import de from './de';
import pt from './pt';

const messages = {
  en,
  id,
  ar,
  pt,
  zh,
  de
};

export default messages;
