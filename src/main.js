import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import i18n from './plugins/i18n';
import fragment from './plugins/vue-fragment';
import mediaQuery from './plugins/vue-media-query';
import slickCarousel from './plugins/caroussel-config';
import countup from './plugins/countup-config';
import lightbox from './plugins/vue-lightbox-config';
import scrollnav from './plugins/vue-scroll-nav';
import gmaps from './plugins/vue-gmaps-config';
import wow from './plugins/vue-wow-config';
import vueformulate from './plugins/vue-formulate-config';

Vue.config.productionTip = false;

Vue.directive('scroll', {
  inserted: (el, binding) => {
    const f = (evt) => {
      if (binding.value(evt, el)) {
        el.removeEventListener('scroll', f);
      }
    };
    el.addEventListener('scroll', f);
  },
});

new Vue({
  router,
  store,
  i18n,
  vuetify,
  fragment,
  mediaQuery,
  slickCarousel,
  countup,
  lightbox,
  scrollnav,
  wow,
  gmaps,
  vueformulate,
  render: (h) => h(App),
}).$mount('#app');
