import Vue from 'vue'
import Slick from 'vue-slick'

const config = {};

Vue.use(Slick);

export default config;
