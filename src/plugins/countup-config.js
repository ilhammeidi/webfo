import Vue from 'vue'
import options from 'vue-countup-directive'

const config = {};

Vue.directive('countUp', options);

export default config;
