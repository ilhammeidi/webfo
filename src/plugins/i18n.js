import Vue from 'vue';
import VueI18n from 'vue-i18n';
import messages from '../locales';

Vue.use(VueI18n);

let defaultLocale = 'en';
if (localStorage.getItem('currentLang')) {
  defaultLocale = localStorage.getItem('currentLang');
}

var locale = window.location.pathname.replace(/^\/([^\/]+).*/i,'$1');

const i18n = new VueI18n({
  locale: defaultLocale, // set locale
  fallbackLocale: 'en',
  messages, // set locale messages
});

export default i18n;
