import Vue from 'vue'
import VueFormulate from '@braid/vue-formulate'
import TextField from '~/components/Forms/Material/TextField';
import Button from '~/components/Forms/Material/Button';
import Select from '~/components/Forms/Material/Select';
import TextArea from '~/components/Forms/Material/TextArea';
import Radio from '~/components/Forms/Material/Radio';
import Checkbox from '~/components/Forms/Material/Checkbox';
import Switcher from '~/components/Forms/Material/Switch';

Vue.component('TextField', TextField);
Vue.component('Button', Button);
Vue.component('Select', Select);
Vue.component('TextArea', TextArea);
Vue.component('Radio', Radio);
Vue.component('Checkbox', Checkbox);
Vue.component('Switcher', Switcher);

const options = {
  library: {
    mtext: {
      classification: 'text',
      component: 'TextField',
      slotProps: {
        component: ['mlabel', 'inputType']
      }
    },
    mbutton: {
      classification: 'button',
      component: 'Button',
      slotProps: {
        component: ['mlabel', 'btnType', 'color']
      }
    },
    mselect: {
      classification: 'select',
      component: 'Select',
      slotProps: {
        component: ['mlabel']
      }
    },
    mtextarea: {
      classification: 'textarea',
      component: 'TextArea',
      slotProps: {
        component: ['mlabel']
      }
    },
    mradio: {
      classification: 'radio',
      component: 'Radio',
    },
    mcheck: {
      classification: 'checkbox',
      component: 'Checkbox',
      slotProps: {
        component: ['mlabel']
      }
    },
    mswitch: {
      classification: 'checkbox',
      component: 'Switcher',
      slotProps: {
        component: ['mlabel']
      }
    },
  }
};

Vue.use(VueFormulate, options);

export default options;
