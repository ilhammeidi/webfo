import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

const config = {
  load: {
    key: '',
    libraries: 'places', // This is required if you use the Autocomplete plugin
  },
  installComponents: true
};

Vue.use(VueGoogleMaps, config);

export default config;
