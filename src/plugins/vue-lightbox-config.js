import Vue from 'vue';
import Lightbox from 'vue-easy-lightbox';

const config = {};

Vue.use(Lightbox);

export default config;
