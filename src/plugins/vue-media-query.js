import Vue from 'vue';
import VueMq from 'vue-mq';

const mediaQuery = {
  breakpoints: { // default breakpoints - customize this
    xsDown: 599,
    xsUp: 600,
    smDown: 959,
    smUp: 960,
    mdDown: 1279,
    mdUp: 1280,
    lgDown: 1919,
    lgUp: 1920,
    xl: Infinity,
  },
  defaultBreakpoint: 'default', // customize this for SSR
};

Vue.use(VueMq, mediaQuery);

export default mediaQuery;
