import Vue from 'vue';
import VueScrollactive from 'vue-scrollactive';

const config = {};

// use default options
Vue.use(VueScrollactive);

export default config;
