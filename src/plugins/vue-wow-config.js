// import
import Vue from 'vue';
import VueWow from 'vue-wow';

const config = {};

// mount with global
Vue.use(VueWow);

export default config;
