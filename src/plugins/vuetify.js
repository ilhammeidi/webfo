import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import ar from 'vuetify/lib/locale/ar';
import en from 'vuetify/lib/locale/en';
import id from 'vuetify/lib/locale/id';
import de from 'vuetify/lib/locale/de';
import pt from 'vuetify/lib/locale/pt';
import zh from 'vuetify/lib/locale/zh-Hans';
import colors from 'vuetify/lib/util/colors';
import i18n from './i18n';

Vue.use(Vuetify);

let themeType = 'light';
let isRtl = 'false';
if (typeof Storage !== 'undefined') {
  themeType = localStorage.getItem('webfoTheme') || 'light';
  isRtl = localStorage.getItem('currentLang') === 'ar';
}

export const palette = {
  cloud: {
    primary: colors.lightBlue.base, // primary main
    primarylight: colors.lightBlue.lighten4, // primary light
    primarydark: colors.lightBlue.darken4, // primary dark
    secondary: colors.orange.base, // secondary main
    secondarylight: colors.orange.lighten4, // secondary light
    secondarydark: colors.orange.darken4, // secondary dark
    accent: colors.indigo.accent4, // secondary main
    accentlight: colors.indigo.lighten4, // secondary light
    accentdark: colors.indigo.darken4, // secondary dark
    anchor: colors.lightBlue.base, // link
  },
  money: {
    primary: colors.green.base, // primary main
    primarylight: colors.green.lighten4, // primary light
    primarydark: colors.green.darken3, // primary dark
    secondary: colors.amber.darken2, // secondary main
    secondarylight: colors.amber.lighten4, // secondary light
    secondarydark: colors.amber.darken4, // secondary dark
    accent: colors.blue.base, // secondary main
    accentlight: colors.blue.lighten4, // secondary light
    accentdark: colors.blue.darken4, // secondary dark
    anchor: colors.green.base, // link
  },
  ocean: {
    primary: colors.blue.base, // primary main
    primarylight: colors.blue.lighten4, // primary light
    primarydark: colors.blue.darken4, // primary dark
    secondary: colors.cyan.base, // secondary main
    secondarylight: colors.cyan.lighten4, // secondary light
    secondarydark: colors.cyan.darken4, // secondary dark
    accent: colors.deepPurple.accent3, // accent main
    accentlight: colors.deepPurple.lighten5, // accent light
    accentdark: colors.deepPurple.darken3, // accent dark
    anchor: colors.blue.base, // link
  },
};

export const theme = {
  ...palette.ocean,
};

export default new Vuetify({
  rtl: isRtl,
  theme: {
    dark: themeType === 'dark',
    options: { customProperties: true },
    themes: {
      light: {
        ...theme,
      },
      dark: {
        ...theme,
      },
    },
  },
  lang: {
    locales: { en, id, ar, zh, pt, de },
    t: (key, ...params) => i18n.t(key, params),
  },
});
