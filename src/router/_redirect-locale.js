import i18n from '~/plugins/i18n';

const urlPath = window.location.pathname;

export const redirectLocale = [
  {
    path: '/',
    redirect: urlPath === '/' ? `/${i18n.locale}` : '/not-found',
  },
  {
    path: '/about',
    redirect: `/${i18n.locale}/about`,
  },
  {
    path: '/pricing',
    redirect: `/${i18n.locale}/pricing`,
  },
  {
    path: '/faq',
    redirect: `/${i18n.locale}/faq`,
  },
  {
    path: '/contact',
    redirect: `/${i18n.locale}/contact`,
  },
  {
    path: '/contact-map',
    redirect: `/${i18n.locale}/contact-map`,
  },
  {
    path: '/login',
    redirect: `/${i18n.locale}/login`,
  },
  {
    path: '/register',
    redirect: `/${i18n.locale}/register`,
  },
  {
    path: '*',
    redirect: `/not-found`,
  },
];

export default redirectLocale;
