import DashboardTemplate from '../components/Template/Dashboard.vue';

const applications = {
  path: '/dashboard',
  component: DashboardTemplate,
  children: [
    {
      name: 'dashboard',
      path: '/dashboard',
      component: () => import('~/views/DashboardView.vue'),
    },
    {
      name: 'tables',
      path: '/dashboard/tables',
      component: () => import('~/views/TableView.vue'),
    },
    {
      name: 'form',
      path: '/dashboard/form',
      component: () => import('~/views/FormView.vue'),
    },
    {
      name: 'blank',
      path: '/dashboard/blank-page',
      component: () => import('~/views/BlankView.vue'),
    },
    {
      name: 'ui',
      path: '/ui-collection',
      component: () => import('~/views/UiView.vue'),
    },
    {
      name: 'dashboard-error',
      path: '/dashboard/*',
      component: () => import('~/views/NotFoundInner.vue'),
    },
  ],
};
      
export default applications;
