import Vue from 'vue';
import VueRouter from 'vue-router';
import Meta from 'vue-meta';
import applications from './applications';
import landing from './landing';
import redirectLocale from './_redirect-locale';
import users from './users';
import NotFound from '../views/NotFound.vue';
import ComingSoon from '../views/ComingSoon.vue';
import Maintenance from '../views/Maintenance.vue';

Vue.use(VueRouter);
Vue.use(Meta);

const routes = [
  ...redirectLocale, // redirectLanding position must on the most of the top routing
  applications, // Put the application on 2nd route position. Because the dashboard or inner page doesn't has locale path url
  {
    path: '/not-found', // Put the not-found page on 3rd route to catch random / 404 page from 'redirectLanding'
    name: 'not-found',
    component: NotFound,
  },
  // Routing from root WITHOUT locale path url
  {
    path: '/maintenance',
    name: 'maintenance',
    component: Maintenance,
  },
  {
    path: '/coming-soon',
    name: 'coming-soon',
    component: ComingSoon,
  },
  // Rest Routing from root WITH locale path url
  landing,
  users,
  // Not Found Page
  {
    path: '*',
    name: 'not-found-global',
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes,
});

export default router;
