import i18n from '~/plugins/i18n';
import LandingTemplate from '../components/Template/Corporate.vue';

const landing = {
  path: '/:locale',
  component: LandingTemplate,
  beforeEnter: (to, from, next) => {
    const locale = to.params.locale; // eslint-disable-line
    const supported_locales = process.env.VUE_APP_I18N_SUPPORTED_LOCALE.split(','); // eslint-disable-line
    if (!supported_locales.includes(locale)) return next('/');
    if (i18n.locale !== locale) {
      i18n.locale = locale;
    }
    return next();
  },
  children: [
    {
      path: '/:locale',
      name: 'home',
      component: () => import('../views/HomeView.vue'),
    },
    {
      path: '/:locale/about',
      name: 'about',
      component: () => import('../views/AboutView.vue'),
    },
    {
      path: '/:locale/pricing',
      name: 'pricing',
      component: () => import('../views/PricingView.vue'),
    },
    {
      path: '/:locale/faq',
      name: 'faq',
      component: () => import('../views/FaqView.vue'),
    },
    {
      path: '/:locale/contact',
      name: 'contact',
      component: () => import('../views/ContactView.vue'),
    },
    {
      path: '/:locale/contact-map',
      name: 'contact-map',
      component: () => import('../views/ContactMapView.vue'),
    },
  ],
};

export default landing;
