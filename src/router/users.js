import i18n from '~/plugins/i18n';
import OuterTemplate from '../components/Template/Outer.vue';

const users = {
  path: '/:locale',
  component: OuterTemplate,
  beforeEnter: (to, from, next) => {
    const locale = to.params.locale; // eslint-disable-line
    const supported_locales = process.env.VUE_APP_I18N_SUPPORTED_LOCALE.split(','); // eslint-disable-line
    if (!supported_locales.includes(locale)) return next('/');
    if (i18n.locale !== locale) {
      i18n.locale = locale;
    }
    return next();
  },
  children: [
    {
      path: '/:locale/login',
      name: 'login',
      component: () => import('../views/LoginView.vue'),
    },
    {
      path: '/:locale/register',
      name: 'register',
      component: () => import('../views/RegisterView.vue'),
    },
    {
      path: '/:locale/reset-password',
      name: 'reset',
      component: () => import('../views/ResetView.vue'),
    },
  ],
};

export default users;
