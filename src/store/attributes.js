const attributes = {
  state: {
    title: 'untitled',
    category: 'default',
  },
  mutations: {
    SET_TITLE(state, payload) {
      state.title = payload;
    },
    SET_CATEGORY(state, payload) {
      state.category = payload;
    },
  },
  actions: {
    setTitle(context, title) {
      context.commit('SET_TITLE', title);
    },
    setCategory(context, category) {
      context.commit('SET_CATEGORY', category);
    },
  },
};

export default attributes;
