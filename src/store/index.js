import Vue from 'vue';
import Vuex from 'vuex';
import uiStore from './ui';
import attrStore from './attributes';
import breakpoints from './breakpoints';

Vue.use(Vuex);

const moduleA = {
  state: {
    countA: 0,
  },
  mutations: {
    INCREMENT_A(state) {
      state.countA += 1;
    },
  },
  actions: {
    incrementA(context) {
      context.commit('INCREMENT_A');
    },
  },
};

const moduleB = {
  state: {
    countB: 10,
  },
  mutations: {
    INCREMENT_B(state, payload) {
      state.countB += payload;
    },
  },
  actions: {
    incrementB(context, val) {
      context.commit('INCREMENT_B', val);
    },
  },
};

export default new Vuex.Store({
  modules: {
    ma: moduleA,
    mb: moduleB,
    ui: uiStore,
    attr: attrStore,
    breakpoints,
  },
});
