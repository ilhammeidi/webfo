let darkMode = false;
if (typeof Storage !== 'undefined') { // eslint-disable-line
  darkMode = localStorage.getItem('webfoDarkMode') || false;
}

const ui = {
  state: {
    dark: darkMode === 'true',
    header: 'basic', /* available: basic, droplist, mega */
    headerLanding: 'mixed', /* available: mixed, basic, navscroll, hamburger */
    footerLanding: 'blog', /* available: sitemap, blog */
    sidebar: 'list', /* available: list, big, no-sidebar */
    expandSidebar: true,
  },
  mutations: {
    SET_HEADER(state, payload) {
      state.header = payload;
    },
    SET_EXPANDED(state, payload) {
      state.expandSidebar = payload;
    },
    SET_DARK(state) {
      state.dark = !state.dark;
    },
  },
  actions: {
    setHeader(context, headerType) {
      context.commit('SET_HEADER', headerType);
    },
    setExpanded(context, expand) {
      context.commit('SET_EXPANDED', expand);
    },
    setDark(context) {
      context.commit('SET_DARK');
    },
  },
};

export default ui;
