const { defineConfig } = require('@vue/cli-service');
const path = require('path');

module.exports = defineConfig({
  lintOnSave: false,
  css: {
    loaderOptions: {
      scss: { additionalData: `@import "~@/assets/scss/main.scss";` } // eslint-disable-line
    }
  },
  runtimeCompiler: true,
  transpileDependencies: [
    'vuetify',
  ],
  configureWebpack: {
    resolve: {
      alias: {
        '~': path.resolve(__dirname, 'src')
      }
    },
  }
});
